
import { IProductA, IProductB, IProduct } from "./interfaces";

export class ComplexObject {
    object: Object[];
    constructor() {
        this.object = [];
    }
    add(obj) {
        this.object.push(obj);
    }
    getObject() {
        return this.object;
    }
}

export class ProductA implements IProductA {
    getName() {
        return 'Product A Name';
    }
}

export class ProductB implements IProductB {
    getName() {
        return 'Product B Name';
    }
}

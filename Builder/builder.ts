import { IBuilder } from "./interfaces";
import { ComplexObject, ProductA, ProductB } from "./products";

export class Builder implements IBuilder {
    complexObject: ComplexObject;
    constructor() {
        this.complexObject = new ComplexObject();
    }

    buildPartA() {
        console.log("Building Part A");
        this.complexObject.add(new ProductA().getName());
    }
    buildPartB() {
        console.log("Building Part B");
        this.complexObject.add(new ProductB().getName());
    }

    getResult() {
        return this.complexObject;
    }
}
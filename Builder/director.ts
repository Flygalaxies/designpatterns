import { Builder } from "./builder";
import { ComplexObject } from "./products";

export class Director {
    builder: Builder;
    complexObject: ComplexObject;
    constructor(builder: Builder) {
        console.log("Director");
        this.builder = builder;
    }
    construct() {
        console.log("Construct Complex Object");
        this.builder.buildPartA();
        this.builder.buildPartB();
        this.complexObject = this.builder.getResult();
        return this.complexObject.getObject();
    }
}
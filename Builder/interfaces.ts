import { ComplexObject } from "./products";
export interface IBuilder {
    buildPartA: () => void;
    buildPartB: () => void;
    getResult: () => ComplexObject;
}

export interface IProduct {
    getName: () => string;
}

export interface IProductA extends IProduct {
}
export interface IProductB extends IProduct {
}
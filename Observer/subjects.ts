import { Observer } from "./observers";

export abstract class Subject {
    observers: Observer[];
    constructor() {
        this.observers = new Array();
    }
    attach(o: Observer) {
        this.observers.push(o);
        //console.log(this.observers);
    }
    notifyObservers() {
        this.observers.forEach(element => {
            element.update();
        });
    }
}

export class Subject1 extends Subject {
    state = 0;
    getState() {
        return this.state;
    }
    setState(state) {
        console.log("state changed from: " + this.state);
        this.state = state;
        console.log("state changed to: " + this.state);

        this.notifyObservers();
    }
}
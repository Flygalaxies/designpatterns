import { Subject1 } from "./subjects";

export abstract class Observer {
    constructor(subject: Subject1) {
        this.subscribe(subject);
    }
    abstract subscribe(subject: Subject1);
    abstract update();
}

export class Observer1 extends Observer {
    subject: Subject1;
    state;
    subscribe(subject: Subject1) {
        this.subject = subject
        this.subject.attach(this);
    }
    update() {
        this.state = this.subject.getState();
        console.log("Observer 1 current state:" + this.state);
    }
}

export class Observer2 extends Observer {
    subject: Subject1;
    state;
    subscribe(subject: Subject1) {
        this.subject = subject
        this.subject.attach(this);
    }
    update() {
        this.state = this.subject.getState();
        console.log("Observer 2 current state:" + this.state);
    }
}
import { IProductA, IProductB } from "./interfaces";

export class ProductA implements IProductA {
    constructor() {}
    getName() {
        return 'Name of A'
    }
}

export class ProductB implements IProductB {
    getProduct(){
        return 'Product of B'
    }
}
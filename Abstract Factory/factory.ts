import { AbstractFactory } from "./abstractFactory";
import { ProductA, ProductB } from "./products";

export class Factory implements AbstractFactory {
    constructor() {
        console.log("Factory Constructor");
    }
    createProductA(): ProductA {
        console.log("Creating Product A");
        return new ProductA();
    }
    createProductB(): ProductB{
        console.log("Creating Product B");
        return new ProductB();
    }
}
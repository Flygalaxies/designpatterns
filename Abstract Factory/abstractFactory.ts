import { IProductA, IProductB } from "./interfaces";

export interface AbstractFactory {
    createProductA: () => IProductA;
    createProductB: () => IProductB;
}
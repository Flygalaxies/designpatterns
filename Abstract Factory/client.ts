import { AbstractFactory } from "./abstractFactory";
import { ProductA, ProductB } from "./products";

export class Client {
    productA: ProductA;
    productB: ProductB;
    factory: AbstractFactory;
    constructor(factory: AbstractFactory) {
        console.log("Client");
        this.factory = factory;
    }
    operation() {
        console.log("Sending Objects to Factory");
        this.productA = this.factory.createProductA();
        this.productB = this.factory.createProductB();
        return `${this.productA.getName()} ; ${this.productB.getProduct()}`;
    }
}
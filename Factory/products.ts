export interface IProduct {
    getName: () => string;
}

export class Product1 implements IProduct {
    constructor() {

    }
    getName() {
        return 'Product 1 name';
    }
}

export class Product2 implements IProduct {
    constructor() {

    }
    getName() {
        return 'Product 2 name';
    }
}
import { IProduct, Product1 } from "./products";

export abstract class Creator {

    product: IProduct;

    abstract factoryMethod(): IProduct;

    constructor() {        
    }

    operation(){
        this.product = this.factoryMethod();
        return this.product.getName();
    }

}

export class Creator1 extends Creator {
    factoryMethod() {
        return new Product1();
    }
}